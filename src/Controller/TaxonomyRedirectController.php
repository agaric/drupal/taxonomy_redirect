<?php

namespace Drupal\taxonomy_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\TermInterface;

/**
 * Provides route responses for taxonomy_redirect.module.
 */
class TaxonomyRedirectController extends ControllerBase {

  /**
   * Redirect to a view.
   */
  public function taxonomyRedirect(TermInterface $taxonomy_term) {
    // Default taxonomy route.
    $route = 'entity.taxonomy_term.canonical';
    switch ($taxonomy_term->getVocabularyId()) {
      case "news":
        $param_name = 'category';
        $route = 'view.search_news_content.page_1';
        break;
      case "curricular_area":
      case "content_type":
      case "education_level":
      case "tools":
        $param_name = $taxonomy_term->getVocabularyId();
        $route = 'view.search_resources_content.page_1';
        break;
      case "forums":
        return $this->redirect('forum.page', ['taxonomy_term' => $taxonomy_term->id()]);
      default:
        // For any other vocabulary, do *not* redirect or we get an infinite loop.
        return $this->redirect('taxonomy_term_redirect.entity.taxonomy_term.canonical',
          ['taxonomy_term' => $taxonomy_term->id()]
        );
    }

    if ($route == 'entity.taxonomy_term.canonical') {
      $route_parameters = ['taxonomy_term' => $taxonomy_term->id()];
      $options = [];
    } else {
      $route_parameters = [];
      $options = [
        'query' => [
          'f[0]' => $param_name . ':' . $taxonomy_term->id(),
        ],
      ];
    }

    return $this->redirect($route, $route_parameters, $options);
  }

}
